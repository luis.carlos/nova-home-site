import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './styles.css';

export default function Header() {
    return (
        <>
            <nav class='navbar navbar-expand-lg navbar-dark fixed-top'>
                <div class='container-fluid'>
                    <a class='navbar-brand' href='#'>
                        <img
                            src='https://www.weg.net/institutional/_ui/desktop/theme-institutional/img/brand-white.svg'
                            alt='logo'
                            width='30'
                            height='30'
                            className='d-inline-block align-top'
                        />
                    </a>
                    <button
                        className='navbar-toggler'
                        type='button'
                        data-bs-toggle='collapse'
                        data-bs-target='#navbarSupportedContent'
                        aria-controls='navbarSupportedContent'
                        aria-expanded='false'
                        aria-label='Toggle navigation'
                    >
                        <span className='navbar-toggler-icon'></span>
                    </button>

                    <div
                        class='collapse navbar-collapse'
                        id='navbarSupportedContent'
                    >
                        <ul class='navbar-nav me-auto mb-2 mb-lg-0'>
                            <li class='nav-item'>
                                <a class='nav-link' href='#'>
                                    A WEG
                                </a>
                            </li>
                            <li className='nav-item'>
                                <a className='nav-link' href='#'>
                                    PRODUTOS
                                </a>
                            </li>
                            <li className='nav-item'>
                                <a className='nav-link' href='#'>
                                    SOLUÇÕES
                                </a>
                            </li>
                            <li className='nav-item'>
                                <a className='nav-link' href='#'>
                                    INVESTIDORES
                                </a>
                            </li>
                            <li className='nav-item'>
                                <a className='nav-link' href='#'>
                                    ÉTICA
                                </a>
                            </li>
                            <li className='nav-item'>
                                <a className='nav-link' href='#'>
                                    SUPORTE
                                </a>
                            </li>
                            <li className='nav-item dropdown'>
                                <a
                                    className='nav-link dropdown-toggle'
                                    href='#'
                                    id='navbarDropdown'
                                    role='button'
                                    data-bs-toggle='dropdown'
                                    aria-expanded='false'
                                >
                                    Dropdown
                                </a>
                                <ul
                                    className='dropdown-menu'
                                    aria-labelledby='navbarDropdown'
                                >
                                    <li>
                                        <a className='dropdown-item' href='#'>
                                            Action
                                        </a>
                                    </li>
                                    <li>
                                        <a className='dropdown-item' href='#'>
                                            Another action
                                        </a>
                                    </li>
                                    <li>
                                        <hr className='dropdown-divider' />
                                    </li>
                                    <li>
                                        <a className='dropdown-item' href='#'>
                                            Something else here
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                        <ul class='navbar-nav d-flex me-1 mb-lg-0'>
                            {/*<ul className="navbar-nav me-auto mb-2 mb-lg-0">*/}
                            <li className='nav-item'>
                                <a className='nav-link' href='#'>
                                    Notícias
                                </a>
                            </li>
                            <li className='nav-item'>
                                <a className='nav-link' href='#'>
                                    Treinamento
                                </a>
                            </li>
                            <li className='nav-item'>
                                <a className='nav-link' href='#'>
                                    Trabalhe Conosco
                                </a>
                            </li>
                        </ul>

                        <form className='w-auto'>
                            <input
                                type='search'
                                className='form-control form-rounded'
                                placeholder='Pesquisa'
                                aria-label='Search'
                            />
                        </form>

                        <ul class='navbar-nav d-flex justify-content-center flex-row me-1'>
                            <li class='nav-item me-3 me-lg-0'>
                                <a class='nav-link' href='#'>
                                    <i className='fa-solid fa-arrow-right-arrow-left'></i>
                                </a>
                            </li>
                            <li class='nav-item me-3 me-lg-0'>
                                <a class='nav-link' href='#'>
                                    <i className='fa-solid fa-user'></i>
                                </a>
                            </li>
                            <li class='nav-item me-3 me-lg-0'>
                                <a class='nav-link' href='#'>
                                    <i className='fa-solid fa-flag'></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    );
}
