import React from 'react';
import '../../sections/Home/styles.css';
import Header from '../../components/header';
import Banner from '../../assets/BANNER-TOPO-01-institucional-A.jpg';

export default function Home() {
    return (
        <>
            <section>
                <Header />
                <div
                    id='carouselExampleControls'
                    className='carousel slide'
                    data-bs-ride='carousel'
                >
                    <div className='carousel-inner'>
                        <div className='carousel-item active'>
                            <img
                                src={Banner}
                                className='d-block w-100'
                                alt='...'
                            />
                            <div className='carousel-caption d-none d-md-block'>
                                <h5>
                                    #SOMOSWEG <br />
                                </h5>
                                <p>
                                    {' '}
                                    Movidos por energia, tecnologia <br />
                                    desafios e oportunidades
                                </p>
                                <button type='button' className='btn btn-dark'>
                                    ASSITIR AO VÍDEO
                                </button>
                            </div>
                        </div>
                        <div className='carousel-item'>
                            <img
                                src={Banner}
                                className='d-block w-100'
                                alt='...'
                            />
                            <div className='carousel-caption d-none d-md-block mt-8'>
                                <h5>
                                    #SOMOSWEG <br />
                                </h5>
                                <p>
                                    Movidos por energia, tecnologia <br />
                                    desafios e oportunidades
                                </p>
                            </div>
                        </div>
                        <div className='carousel-item'>
                            <img
                                src={Banner}
                                className='d-block w-100'
                                alt='...'
                            />
                            <div className='carousel-caption d-none d-md-block'>
                                <h5>
                                    #SOMOSWEG <br />
                                </h5>
                                <p>
                                    Movidos por energia, tecnologia <br />
                                    desafios e oportunidades
                                </p>
                            </div>
                        </div>
                    </div>
                    <button
                        className='carousel-control-prev'
                        type='button'
                        data-bs-target='#carouselExampleControls'
                        data-bs-slide='prev'
                    >
                        <span className='carousel-control-prev-icon' aria-hidden='true'></span>
                        <span className='visually-hidden'>Previous</span>
                    </button>
                    <button
                        className='carousel-control-next'
                        type='button'
                        data-bs-target='#carouselExampleControls'
                        data-bs-slide='next'
                    >
                        <span className='carousel-control-next-icon' aria-hidden='true'></span>
                        <span className='visually-hidden'>Next</span>
                    </button>
                </div>
            </section>
        </>
    );
}
