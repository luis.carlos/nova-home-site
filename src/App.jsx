import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Home from './sections/Home';

function App() {
  return (
    <div className='App'>
      <Home />
    </div>
  );
}

export default App;
